﻿namespace Student_Managerment_EFCore
{
    public class StudentManagementMenu
    {
        public void MainMenu()
        {
            Console.WriteLine("==============MENU==============");
            Console.WriteLine("| 1. View All Student          |");
            Console.WriteLine("| 2. Find Student by name      |");
            Console.WriteLine("| 3. Add new student           |");
            Console.WriteLine("| 4. Update student by id      |");
            Console.WriteLine("| 5. Remove student by id      |");
            Console.WriteLine("| 6. View percentage of gender |");
            Console.WriteLine("| 7. Exit                      |");
            Console.WriteLine("================================");
            Console.Write("Choose: ");
        }

        public void PercentageGenderMenu()
        {
            Console.WriteLine("==========PERCENTAGE=MENU==========");
            Console.WriteLine("| 1. Percentage of male           |");
            Console.WriteLine("| 2. Percentage of female         |");
            Console.WriteLine("| 3. Exit                         |");
            Console.WriteLine("===================================");
        }

        public void Update()
        {
            Console.WriteLine("===========UPDATE===========");
            Console.WriteLine("| 1. Edit Name             |");
            Console.WriteLine("| 2. Edit Major            |");
            Console.WriteLine("| 3. Edit Gender           |");
            Console.WriteLine("| 4. Exit                  |");
            Console.WriteLine("============================");
        }
    }
}
