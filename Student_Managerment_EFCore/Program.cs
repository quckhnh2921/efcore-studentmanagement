﻿using Student_Managerment_EFCore;
using static Student_Managerment_EFCore.GenderEnum;

StudentController controller = new StudentController();
StudentManagementMenu menu = new StudentManagementMenu();
int choice = 0;
do
{
    try
    {
        menu.MainMenu();
        choice = int.Parse(Console.ReadLine());
        switch (choice)
        {
            case 1:
                var viewAll = controller.ViewAllStudent();
                Console.WriteLine(viewAll);
                break;
            case 2:
                Console.WriteLine("Enter name of student: ");
                string studentName = Console.ReadLine();
                var students = controller.FindStudentByName(studentName);
                Console.WriteLine(students);
                break;
            case 3:
                Console.WriteLine("Enter Student name: ");
                string name = Console.ReadLine();
                Console.WriteLine("Enter Student major: ");
                string major = Console.ReadLine();
                Console.WriteLine("Enter Student gender(Male or Female): ");
                string genderInput = Console.ReadLine();
                if (!Enum.TryParse<Gender>(genderInput, true, out Gender gender))
                {
                    throw new Exception("Gender is invalid");
                }
                Student student = new Student(name, major, gender);
                controller.AddStudent(student);
                break;
            case 4:
                Console.WriteLine("Enter Student id: ");
                int studentId = int.Parse(Console.ReadLine());
                int updateChoice = 0;
                var updateStudent = controller.FindStudentByID(studentId);
                if(updateStudent is not null)
                {
                    do
                    {
                        menu.Update();
                        updateChoice = int.Parse(Console.ReadLine());
                        controller.UpdateStudent(updateStudent, updateChoice);
                    } while (updateChoice != 4);
                }
                break;
            case 5:
                Console.WriteLine("Enter Student id: ");
                int studentIdRemove = int.Parse(Console.ReadLine());
                controller.RemoveStudent(studentIdRemove);
                break;
            case 6:
                int subchoice = 0;
                do
                {
                    menu.PercentageGenderMenu();
                    subchoice = int.Parse(Console.ReadLine());
                    switch (subchoice)
                    {
                        case 1:
                            var male = controller.PercentageMale();
                            Console.WriteLine($"{male}%");
                            break;
                        case 2:
                            var female = controller.PercentageFemale();
                            Console.WriteLine($"{female}%");
                            break;
                        default:
                            break;
                    }
                } while (subchoice != 3);
                break;
            case 7:
                Environment.Exit(0);
                break;
            default:
                throw new Exception("Please choose in range 1-7");
        }
    }
    catch (Exception ex)
    {
        Console.WriteLine(ex.Message);
    }
} while (choice != 7);