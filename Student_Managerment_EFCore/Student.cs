﻿using System.ComponentModel.DataAnnotations;

namespace Student_Managerment_EFCore
{
    public class Student
    {
        public Student(string name, string major, GenderEnum.Gender gender)
        {
            Name = name;
            Major = major;
            Gender = gender;
        }
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Major { get; set; }
        [Required]
        public GenderEnum.Gender Gender { get; set; }
    }
}
