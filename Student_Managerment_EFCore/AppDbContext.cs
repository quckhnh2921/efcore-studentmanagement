﻿using Microsoft.EntityFrameworkCore;
namespace Student_Managerment_EFCore
{
    public class AppDbContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var conectionString = "Data Source = KHANHS\\SQLSERVER; Database = Student_Management_EFCore; Trusted_Connection = True; TrustServerCertificate = True";
            optionsBuilder.UseSqlServer(conectionString);
        }
    }
}
