﻿using System.Text;
using static Student_Managerment_EFCore.GenderEnum;
namespace Student_Managerment_EFCore
{
    public class StudentController
    {
        private const int UPDATE_NAME = 1;
        private const int UPDATE_MAJOR = 2;
        private const int UPDATE_GENDER = 3;
        private readonly AppDbContext context;
        public StudentController()
        {
            context = new AppDbContext();
        }

        public void AddStudent(Student student)
        {
            context.Students.Add(student);
            context.SaveChanges();
        }

        public string ViewAllStudent()
        {
            StringBuilder sb = new StringBuilder();
            var students = context.Students.ToList();
            if (students.Count is 0)
            {
                throw new Exception("Empty list");
            }
            foreach (var student in students)
            {
                sb.Append($"\n{student.Id} - {student.Name} - {student.Major} - {student.Gender}");
            }
            return sb.ToString();
        }

        public void RemoveStudent(int studentId)
        {
            var student = context.Students.FirstOrDefault(student => student.Id == studentId);
            if (student is null)
            {
                throw new Exception("Can not found the student with id: " + studentId);
            }
            context.Students.Remove(student);
            context.SaveChanges();
            Console.WriteLine("Remove successful!");
        }
        public Student FindStudentByID(int studentID)
        {
            var student = context.Students.FirstOrDefault(student => student.Id == studentID);
            if (student is null)
            {
                throw new Exception($"Student id {studentID} is not found");
            }
            return student;
        }
        private void UpdateDetails(Student student, int updateChoice)
        {
            if (updateChoice == UPDATE_NAME)
            {
                Console.WriteLine("Enter student name: ");
                string name = Console.ReadLine();
                student.Name = name;
            }
            if (updateChoice == UPDATE_MAJOR)
            {
                Console.WriteLine("Enter student major: ");
                string major = Console.ReadLine();
                student.Major = major;
            }
            if (updateChoice == UPDATE_GENDER)
            {
                Console.WriteLine("Enter student gender(Male or Female): ");
                string newGender = Console.ReadLine();
                if (!Enum.TryParse<Gender>(newGender, true, out Gender gender))
                {
                    throw new Exception("Update fail");
                }
                student.Gender = gender;
            }
        }
        public void UpdateStudent(Student student, int updateChoice)
        {
            UpdateDetails(student, updateChoice);
            context.SaveChanges();
            Console.WriteLine("Update successful!");
        }

        public string FindStudentByName(string name)
        {
            StringBuilder sb = new StringBuilder();
            var students = context.Students.Where(student => student.Name.Equals(name)).ToList();
            if (students.Count == 0)
            {
                throw new Exception($"Student {name} does not exist !");
            }
            foreach (var student in students)
            {
                sb.Append($"\n{student.Id} - {student.Name} - {student.Major} - {student.Gender}");
            }
            return sb.ToString();
        }

        public double PercentageMale()
        {
            double totalStudent = context.Students.Count();
            double maleStudent = context.Students.Count(student => student.Gender == Gender.Male);
            return Math.Round((maleStudent / totalStudent) * 100, 2);
        }

        public double PercentageFemale()
        {
            double totalStudent = context.Students.Count();
            double femaleStudent = context.Students.Count(student => student.Gender == Gender.Female);
            return Math.Round((femaleStudent / totalStudent) * 100, 2);
        }
    }
}
